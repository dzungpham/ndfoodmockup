''' Implements handler for /items
Imported from handler for /categories/{id} '''
import logging
import cherrypy
import mysql.connector
import sys
sys.stdout = sys.stderr # Turn off console output; it will get logged by Apache
import os
import os.path
import json
import pprint
from jinja2 import Environment, FileSystemLoader
env = Environment(loader=FileSystemLoader(os.path.abspath(os.path.dirname(__file__))+'/templates/'))
from itemid import ItemID

class Items(object):
    ''' Handles resources /restaurants{restID}/categories/{catID}/items}
        Allowed methods: GET, POST, OPTIONS  '''
    exposed = True

    def __init__(self):
        self.id = ItemID()
        self.db=dict()
        self.db['name']='FeedND'
        self.db['user']='root'
        self.db['host']='127.0.0.1'

    def getDataFromDB(self,catID):
        cnx = mysql.connector.connect(user=self.db['user'],host=self.db['host'],database=self.db['name'])
        cursor = cnx.cursor()
        qn="select name from categories where categoryID=%s" % catID
        cursor.execute(qn)
        catName=cursor.fetchone()[0]
        q="select itemID, name, description, price from items where categoryID=%s" % catID
        cursor.execute(q)
        result=cursor.fetchall()
        return catName, result

    def GET(self, restID, catID):
        ''' Return list of items for categories for restaurant id'''
        # Return data in the format requested in the Accept header
        # Fail with a status of 406 Not Acceptable if not HTML or JSON
        output_format = cherrypy.lib.cptools.accept(['text/html', 'application/json'])

        try:
            catName,result=self.getDataFromDB(catID)
            pp=pprint.PrettyPrinter(indent=6)
            pp.pprint(result)
        except Error as e:
            logging.error(e)
            raise
        
        if output_format == 'text/html':
            return env.get_template('items-tmpl.html').render(rID=restID,cID=catID,cName=catName,items=result,base=cherrypy.request.base.rstrip('/') + '/')
        else:
            data = [{
                    'href': '/restaurants/%s/categories/%s/items/%s' % (restID, catID, itemID),
                    'name': itemName,
                    'description': desc,
                    'price' : unicode(price)
                    } for itemID, itemName, desc, price in result]
            return json.dumps(data, encoding='utf-8')
        
    def POST(self, restID, catID,  **kwargs):
        result= "POST /restaurants/{restID=%s}/categories/{catID=%s}/items     ...     Items.POST\n" % (restID,catID)
        result+= "POST /restaurants/{restID=%s}/categories/{catID=%s}/items body:\n" % (restID, catID)
        for key, value in kwargs.items():
            result+= "%s = %s \n" % (key,value)
        # Validate form data
        # Insert restaurant
        # Prepare response
        return result

    def OPTIONS(self,restID, catID):
        return "<p>/restaurants/{restID=%s}/categories/{catID=%s}/items allows GET, POST, and OPTIONS</p>"


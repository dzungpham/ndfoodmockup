import requests
s = requests.Session()
host='http://ec2-52-0-27-123.compute-1.amazonaws.com'   #  replace by your host here
s.headers.update({'Accept': 'application/json'})
r = s.get('http://ec2-52-0-27-123.compute-1.amazonaws.com/restaurants',)
print r.status_code
if r.status_code == requests.codes.ok:
    print(r.json())

r = s.post('http://ec2-52-0-27-123.compute-1.amazonaws.com/restaurants')
print '\n', r.status_code
if r.status_code == requests.codes.ok:
    print(r.text)


r = s.get('http://ec2-52-0-27-123.compute-1.amazonaws.com/restaurants/11')
print '\n', r.status_code
if r.status_code == requests.codes.ok:
    print(r.text)

r = s.get('http://ec2-52-0-27-123.compute-1.amazonaws.com/restaurants/11/categories')
print '\n', r.status_code
if r.status_code == requests.codes.ok:
    print(r.json())


r = s.get('http://ec2-52-0-27-123.compute-1.amazonaws.com/restaurants/11/categories/82')
print '\n', r.status_code, r.text


r = s.get('http://ec2-52-0-27-123.compute-1.amazonaws.com/restaurants/11/categories/82/items')
print '\n', r.status_code
if r.status_code == requests.codes.ok:
    print(r.json())

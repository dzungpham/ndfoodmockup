#Use this script to create the database tables
#This script does NOT populate the tables with any data

import mysql.connector

#Define database variables
DATABASE_USER = 'root'
DATABASE_HOST = '127.0.0.1'
DATABASE_NAME = 'FeedND'

#Create connection to MySQL
cnx = mysql.connector.connect(user=DATABASE_USER, host=DATABASE_HOST, use_unicode=True)
cursor = cnx.cursor()

###################################
## Create DB if it doesn't exist ##
###################################

createDB = (("CREATE DATABASE IF NOT EXISTS %s DEFAULT CHARACTER SET utf8mb4") % (DATABASE_NAME))
cursor.execute(createDB)

#########################
## Switch to FeedND DB ##
#########################

useDB = (("USE %s") % (DATABASE_NAME))
cursor.execute(useDB)

###########################
## Drop all tables first ##
###########################

#OrderItems
dropTableQuery = ("DROP TABLE IF EXISTS orderItems")
cursor.execute(dropTableQuery)

#Orders
dropTableQuery = ("DROP TABLE IF EXISTS orders")
cursor.execute(dropTableQuery)

#Users
dropTableQuery = ("DROP TABLE IF EXISTS users")
cursor.execute(dropTableQuery)

#Items
dropTableQuery = ("DROP TABLE IF EXISTS items")
cursor.execute(dropTableQuery)


#Categories
dropTableQuery = ("DROP TABLE IF EXISTS categories")
cursor.execute(dropTableQuery)

#Restaurants
dropTableQuery = ("DROP TABLE IF EXISTS restaurants")
cursor.execute(dropTableQuery)

########################
## Create tables next ##
########################

#Users
createTableQuery = ("CREATE TABLE users ("
						"userID INT NOT NULL AUTO_INCREMENT,"
						"name VARCHAR(45) CHARACTER SET utf8mb4  NOT NULL,"
						"email VARCHAR(45)  CHARACTER SET utf8mb4 NOT NULL,"
						"password VARCHAR(45)  CHARACTER SET utf8mb4 NOT NULL,"
						"phone VARCHAR(20) CHARACTER SET utf8mb4  NOT NULL,"
                                                "UNIQUE KEY (email) USING BTREE,"
						"PRIMARY KEY (userID))")
cursor.execute(createTableQuery)

#Restaurants
createTableQuery = ("CREATE TABLE restaurants ("
						"restID INT NOT NULL AUTO_INCREMENT,"
                                                "locuID VARCHAR(20)  CHARACTER SET utf8mb4  NOT NULL,"
						"name VARCHAR(45) CHARACTER SET utf8mb4  NOT NULL,"
						"address VARCHAR(100)  CHARACTER SET utf8mb4 NOT NULL,"
						"city VARCHAR(45)  CHARACTER SET utf8mb4 NOT NULL,"
						"state VARCHAR(20)  CHARACTER SET utf8mb4  NOT NULL,"
						"zip VARCHAR(5)  CHARACTER SET utf8mb4 NOT NULL,"
						"phone VARCHAR(20)  CHARACTER SET utf8mb4 NOT NULL,"
						"lat DECIMAL(10,8) NOT NULL,"
						"lng DECIMAL(11,8) NOT NULL,"
                                                "url VARCHAR(100)  CHARACTER SET utf8mb4 ,"
                                                "UNIQUE KEY (locuID) using BTREE,"
						"PRIMARY KEY (restID))")
cursor.execute(createTableQuery)

#Categories
createTableQuery = ("CREATE TABLE categories ("
						"categoryID INT NOT NULL AUTO_INCREMENT,"
						"restID INT NOT NULL,"
						"name VARCHAR(100) CHARACTER SET utf8mb4  NOT NULL,"
                                                "FOREIGN KEY (restID) REFERENCES restaurants(restID) on delete cascade,"
                                                "UNIQUE KEY (restID,name) USING BTREE,"
						"PRIMARY KEY (categoryID))")
cursor.execute(createTableQuery)

#Items
createTableQuery = ("CREATE TABLE items ("
						"itemID INT NOT NULL AUTO_INCREMENT,"
						"categoryID INT NOT NULL,"
						"name VARCHAR(100) CHARACTER SET utf8mb4 NOT NULL,"
						"description VARCHAR(500) CHARACTER SET utf8mb4 ,"
						"price DECIMAL(5,2),"
                                                "FOREIGN KEY (categoryID) REFERENCES categories(categoryID) on delete cascade,"
                                                "UNIQUE KEY (categoryID,name) USING BTREE,"
						"PRIMARY KEY (itemID))")
cursor.execute(createTableQuery)

#Orders
createTableQuery = ("CREATE TABLE orders ("
						"orderID INT NOT NULL AUTO_INCREMENT,"
						"userID INT NOT NULL,"
						"lastUpdated DATE NOT NULL,"
						"placed INT NOT NULL,"
												"FOREIGN KEY (userID) references users(userID) on delete cascade,"
						"PRIMARY KEY (orderID))")
cursor.execute(createTableQuery)

#OrderItems
createTableQuery = ("CREATE TABLE orderItems ("
						"orderItemID INT NOT NULL AUTO_INCREMENT,"
						"orderID  INT NOT NULL,"
						"itemID INT NOT NULL,"
						"quantity INT NOT NULL,"
                                                "FOREIGN KEY (orderID) references orders(orderID) on delete cascade,"
                                                "FOREIGN KEY (itemID) references items(itemID) on delete cascade,"
                                                "UNIQUE KEY (orderID,itemID) USING BTREE,"
						"PRIMARY KEY (orderItemID))")
cursor.execute(createTableQuery)

#Commit the data and close the connection to MySQL
cnx.commit()
cnx.close()

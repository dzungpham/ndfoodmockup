#Use this script to populate the |restaurant| and |items| tables of the database

import mysql.connector
from mysql.connector import Error
import json
from decimal import *
import random
import logging
import sys

logging.basicConfig(filename='mysql.log', level=logging.DEBUG, 
                    format='%(asctime)s %(message)s')

#Helper functions
def getMenuFromList(menus):
	if len(menus) == 1:
		return menus[0]
	else:
		for menu in menus:
			if menu['menu_name'] in ('Menu', 'Dinner', 'Food', 'Regular', 'Burger King'):
				return menu

#Define database variables
DATABASE_USER = 'root'
DATABASE_HOST = '127.0.0.1'
DATABASE_NAME = 'FeedND'

#Create connection to MySQL
cnx = mysql.connector.connect(user=DATABASE_USER, host=DATABASE_HOST, database=DATABASE_NAME, 
			      charset='utf8mb4',
			      use_unicode=True)
cursor = cnx.cursor()

#Load json file
inputFile = open('restaurantData.json','r')
restaurantDict = json.load(inputFile)
inputFile.close()

restaurantsFile = open('restaurants.txt','w')
categoriesFile = open('categories.txt','w')
itemsFile = open('items.txt','w')
orphansFile = open('orphans.txt','w')

restaurantID = 0
categoryID = 0
orphanItems = 0
#Loop through the restaurants and add info and menu to database
for key, restaurant in restaurantDict.iteritems():
	
	###############################
	## Add restaurant info first ##
	###############################

	inputDict = {
		'locuID' : key,
		'name' : restaurant['name'],
		'address' : restaurant['street_address'],
		'city' : restaurant['locality'],
		'state' : restaurant['region'],
		'zip' : restaurant['postal_code'],
		'phone' : restaurant['phone'],
		'lat' : restaurant['lat'],
		'lng' : restaurant['long'],
		'url' : restaurant['website_url']
	}

	#Insert this info into the database
	addRestaurant = ("INSERT INTO restaurants (locuID, name, address, city, state, zip, phone, lat, lng, url) VALUES (%(locuID)s, %(name)s, %(address)s, %(city)s, %(state)s, %(zip)s, %(phone)s, %(lat)s, %(lng)s, %(url)s)")
	cursor.execute(addRestaurant,inputDict)

	restaurantsFile.write(str(inputDict)+'\n\n')

	#We need to get the restaurantId of the restaurant we just inserted. We'll need this later for the INSERT of the menu
	restID = cursor.lastrowid
	print "restID %s" % restID

	########################
	## Add menu info next ##
	########################

	#1. We need to grab a menu from the |restaurant|
	menu = getMenuFromList(menus = restaurant['menus'])

	#2. We run through the sections of the menu and add them to our |category| table
	sections = menu['sections']
	for section in sections:
		inputDict = {
			'restID' : restID,
			'name' : section['section_name'].replace(u"\u200B","")
		}
		try:
			addCategoryForRestaurant = ("INSERT INTO categories (restID, name) VALUES (%(restID)s, %(name)s)")
			cursor.execute(addCategoryForRestaurant,inputDict)
			categoriesFile.write(str(inputDict)+'\n\n')

		except Error as e:
			logging.error(e)  # log if there are duplicates inserted or some other error

		categoryID = cursor.lastrowid

		#3. For each category, we insert the items of that category into our |items| table
		subsections = section['subsections']
		for subsection in subsections:
			contents = subsection['contents']
			for item in contents:
				if item['type'] == 'ITEM':
					#restaurantID, categoryID, name
					inputDict = {
						'categoryID' : categoryID,
						'name' : item['name']
					}

					#description
					if item.has_key('description'):
					 	inputDict['description'] = item['description'].replace(u"\u200B","")
					else:
					 	inputDict['description'] = ' '

					#price
					isOrphan = False
					if item.has_key('price'):
						try:
							inputDict['price'] = Decimal(item['price'])
						except InvalidOperation:
							price = str(random.randint(5, 20)) + '.99'
							inputDict['price'] = Decimal(price)
							orphanItems+=1
							isOrphan = True
					else:
						price = str(random.randint(5, 20)) + '.99'
						inputDict['price'] = Decimal(price)
						orphanItems+=1
						isOrphan = True

#					Insert item in |items| table
					try:
						addItemForRestaurantAndCategory = ("INSERT INTO items (categoryID,price,name,description) VALUES (%(categoryID)s, %(price)s, %(name)s, %(description)s)")
						cursor.execute(addItemForRestaurantAndCategory,inputDict)
						itemsFile.write(str(inputDict)+'\n\n')
						if isOrphan:
							orphansFile.write(str(inputDict)+'\n\n')
					except Error as e:
						logging.error(e)  # log if there are duplicates inserted or some other error
#Cleanup of data
cquery=("delete from categories where categoryID=42;")
cursor.execute(cquery)
cquery=("delete from categories where categoryID=100;")
cursor.execute(cquery)
#Close aux files
itemsFile.close()
categoriesFile.close()
restaurantsFile.close()
print "Orphan items %s" % str(orphanItems)
#Commit
cnx.commit()
cnx.close()



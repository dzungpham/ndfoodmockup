{'city': u'South Bend', 'locuID': u'0559f40309a19b3949d5', 'url': u'http://www.macrifamily.com/', 'name': u"Macri's Italian Bakery", 'zip': u'46617', 'address': u'214 North Niles Ave.', 'lat': 41.678254, 'lng': -86.244274, 'state': u'IN', 'phone': u'(574) 282-1010'}

{'city': u'South Bend', 'locuID': u'00dae7092df41d0f8df2', 'url': u'http://www.mandarinhousein.com', 'name': u'Mandarin House', 'zip': u'46617', 'address': u'2104 Edison Rd.', 'lat': 41.694324, 'lng': -86.21752261, 'state': u'IN', 'phone': u'(574) 287-4414'}

{'city': u'South Bend', 'locuID': u'e3f1400c66b37c410723', 'url': u'http://club-23.com', 'name': u'Club 23', 'zip': u'46617', 'address': u'744 N. Notre Dame Ave.', 'lat': 41.68504, 'lng': -86.237843, 'state': u'IN', 'phone': u'(574) 234-4015'}

{'city': u'South Bend', 'locuID': u'4218edc2d112582bde38', 'url': u'http://sunnyitalycafe.com', 'name': u'Sunny Italy Cafe', 'zip': u'46617', 'address': u'601 N. Niles Ave.', 'lat': 41.68338635, 'lng': -86.24484385, 'state': u'IN', 'phone': u'(574) 232-9620'}

{'city': u'South Bend', 'locuID': u'4a1ecd9a41ed6402b8ba', 'url': u'http://morrisinn.nd.edu/dining/sorins/', 'name': u"Sorin's", 'zip': u'46556', 'address': u'1 Notre Dame Ave.', 'lat': 41.756634, 'lng': -86.238832, 'state': u'IN', 'phone': u'(574) 631-2020'}

{'city': u'South Bend', 'locuID': u'73a2615c8f11cf22ac84', 'url': u'http://eastbankemporium.com', 'name': u'The Emporium', 'zip': u'46617', 'address': u'121 S. Niles Ave.', 'lat': 41.676031, 'lng': -86.244344, 'state': u'IN', 'phone': u'(574) 234-9000'}

{'city': u'South Bend', 'locuID': u'24419e195104ad57ba86', 'url': u'http://www.southbendbarnabys.com', 'name': u"Barnaby's", 'zip': u'46617', 'address': u'713 E. Jefferson Blvd.', 'lat': 41.6752998757955, 'lng': -86.2409442805595, 'state': u'IN', 'phone': u'(574) 288-4981'}

{'city': u'Notre Dame', 'locuID': u'f86eefdd5c83f1d31e5e', 'url': u'http://www.starbucks.com/', 'name': u'Starbucks', 'zip': u'46556', 'address': u'217 South Dining Hall', 'lat': 41.705692, 'lng': -86.248883, 'state': u'IN', 'phone': u'(574) 631-6902'}

{'city': u'Notre Dame', 'locuID': u'0196cde7f90c65f47ab2', 'url': u'http://www.bk.com/', 'name': u'Burger King', 'zip': u'46556', 'address': u'University Of Notre Dame', 'lat': 41.712139, 'lng': -86.250148, 'state': u'IN', 'phone': u'(219) 631-6902'}

{'city': u'South Bend', 'locuID': u'fec9589dfff42b2e6d10', 'url': u'http://morrisinn.nd.edu/dining/rohrs/', 'name': u"Rohr's", 'zip': u'46556', 'address': u'1 Notre Dame', 'lat': 41.756634, 'lng': -86.238832, 'state': u'IN', 'phone': u'(574) 631-2018'}

{'city': u'Notre Dame', 'locuID': u'7009c6cc1083e4c6b3dd', 'url': u'http://www.subway.com/', 'name': u'Subway', 'zip': u'46556', 'address': u'Lafortune Student Ctr', 'lat': 41.702043, 'lng': -86.237645, 'state': u'IN', 'phone': u'(574) 631-6902'}

{'city': u'South Bend', 'locuID': u'a2c367a73b6181fec216', 'url': u'http://www.orourkespubhouse.com', 'name': u"O'Rourke's Public House", 'zip': u'46617', 'address': u'1044 Angela Blvd.', 'lat': 41.6926410945102, 'lng': -86.2358593766919, 'state': u'IN', 'phone': u'(574) 251-0355'}

{'city': u'Notre Dame', 'locuID': u'55ea8e237866103a26ef', 'url': u'http://www.legendsofnotredame.org/', 'name': u'Legends of Notre Dame', 'zip': u'46556', 'address': u'100 Legends', 'lat': 41.707119, 'lng': -86.251339, 'state': u'IN', 'phone': u'(574) 631-2582'}

{'city': u'South Bend', 'locuID': u'ce682628b99cd9422e50', 'url': u'http://www.backer-nd.com', 'name': u'Linebacker Lounge', 'zip': u'46617', 'address': u'1631 S. Bend Ave.', 'lat': 41.6941933038149, 'lng': -86.2247800827026, 'state': u'IN', 'phone': u'(574) 289-0186'}

{'city': u'South Bend', 'locuID': u'85cf71a488ac45e0012b', 'url': u'http://themarkdineandtap.com/', 'name': u'The Mark Dine & Tap', 'zip': u'46617', 'address': u'1234 Eddy St.', 'lat': 41.691776, 'lng': -86.235425, 'state': u'IN', 'phone': u'(574) 204-2767'}

{'city': u'South Bend', 'locuID': u'f98dc7678100784d08c2', 'url': u'http://www.ciaosrestaurant.com/', 'name': u"ciao's", 'zip': u'46617', 'address': u'501 North Niles Ave.', 'lat': 41.682661, 'lng': -86.24455, 'state': u'IN', 'phone': u'(574) 289-2426'}


''' Controller for /orders/{orderID}/items/{orderItemsID}'''
import cherrypy
from items import Items

class OrderItemID(object):
    ''' Handles resources /orders/{orderID}
        Allowed methods: GET, PUT, DELETE '''
    exposed = True

    def GET(self, orderID, itemID):
        return "GET /orders/{orderID=%s}/items/{itemID=%s}   ...   OrderItemID.GET" % (orderID,itemID)

    def PUT(self, orderID, itemID, **kwargs):
        result = "PUT /orders/{orderID=%s}/items/{itemID=%s}   ...     OrderItemID.PUT\n" % (orderID,itemID)
        result += "PUT /orders/{orderID=%s}/items/{itemID=%s} body:\n" % (orderID,itemID)
        for key, value in kwargs.items():
            result+= "%s = %s \n" % (key,value)
        # Validate form data
        # Insert or update order
        # Prepare response
        return result

    def DELETE(self, orderID, itemID):
        #Validate id
        #Delete order
        #Prepare response
        return "DELETE /orders/{orderID=%s}/items/{itemID=%s}   ...   OrderItemID.DELETE" % (orderID,itemID)

    def OPTIONS(self, restID, catID, itemID):
        #Prepare response
        return "<p>/orders/{orderID}/items/{itemID} allows GET, PUT, and DELETE</p>"


import cherrypy
import sys
import mysql.connector
import collections

class ExampleApp(object):
    @cherrypy.expose
    def index(self):
        return """<html>
            <head><h1>NDFood Mockup</h1></head>
            <body>
                <ul><a href='#'>Orders</a></ul>
                <ul><a href='#'>Restaurants</a></ul>
                <ul><a href='#'>Account</a></ul>
                <table style="width:20%">""" + self.getFood() + self.close()
            
    def close(self):
        return "</table></body></html>"

    def getFoodLibrary(self):
        foodLibrary = collections.OrderedDict()
        foodLibrary["Mandarin House"] = "1434 m"
        foodLibrary["Sunny Italy Cafe"] = "1777 m"
        foodLibrary["Club 23"] = "1481 m"
        return foodLibrary

    def getFood(self):
        food_element = ''
        food = self.getFoodLibrary()
        for k, v in food.items():
            food_element += "<tr><td>"+str(k)+"</td><td>"+str(v)+"</td>"
        return food_element

    @cherrypy.expose
    def showdb(self):
        cnx = mysql.connector.connect(user='test', password='mypass',
                              host='127.0.0.1',
                              database='testdb')
        cursor = cnx.cursor()
        query = ("SELECT firstname,lastname,email FROM Invitations")
        cursor.execute(query)
        info = str()
        print cursor
        for (firstname, lastname, email) in cursor:
           info = info + "Full Name:" + lastname + firstname + "Email: "+email
        return info
application = cherrypy.Application(ExampleApp(), None)

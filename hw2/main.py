import cherrypy
import sys
import mysql.connector
import collections
from mysql.connector import Error

songs = {
    '1': {
        'title': 'Lumberjack Song',
        'artist': 'Canadian Guard Choir'
    },

    '2': {
        'title': 'Always Look On the Bright Side of Life',
        'artist': 'Eric Idle'
    },

    '3': {
        'title': 'Spam Spam Spam',
        'artist': 'Monty Python'
    }
}

class ExampleApp(object):
    @cherrypy.expose
    def index(self):
        return """<html>
            <head><h1>NDFood Mockup</h1></head>
            <body>
                <ul><a href='#'>Orders</a></ul>
                <ul><a href='#'>Restaurants</a></ul>
                <ul><a href='../ndfoodmockup/hw2/query.py'>Account</a></ul>
                <table style="width:100%">""" + self.getData() + self.close()
    
    # Close tag for the table
    @cherrypy.expose
    def close(self):
        return "</table></body></html>" 

    # Fetch data from the database 
    @cherrypy.expose
    def getData(self):
        data = ""
        cnx = mysql.connector.connect(user='root',host='127.0.0.1', database='lectures')
        cursor = cnx.cursor() 
        query = '''SELECT name,address,city,state,zip,phone,lat,lng,url FROM restaurants'''
        cursor.execute(query)

        for elements in cursor:
            data += "<tr>"
            for element in elements:
                data += "<td>" + str(element) + "</td>"
            data += "</tr>"
        cursor.close()
        cnx.close()

        return data

    @cherrypy.expose
    def GET(self, id=None):
        if id == None:
            return('Here are all the songs we have: %s' % songs)
        elif id in songs:
            song = songs[id]
            return('Song with the ID %s is called %s, and the artist is %s' % (id, song['title'], song['artist']))
        else:
            return('No song with the ID %s :-(' % id)


    # Display some personal information on the homepage 
    @cherrypy.expose
    def showdb(self):
        cnx = mysql.connector.connect(user='test', password='mypass',
                              host='127.0.0.1',
                              database='testdb')
        cursor = cnx.cursor()
        query = ("SELECT firstname,lastname,email FROM Invitations")
        cursor.execute(query)
        info = str()
        print cursor
        for (firstname, lastname, email) in cursor:
           info = info + "Full Name:" + lastname + firstname + "Email: "+email
        return info

application = cherrypy.Application(ExampleApp(), None)

# if __name__ == '__main__':

#     cherrypy.tree.mount(
#         ExampleApp(), '/songs',
#         {'/':
#             {'request.dispatch': cherrypy.dispatch.MethodDispatcher()}
#         }
# ]   )

#     cherrypy.engine.start()
#     cherrypy.engine.block()

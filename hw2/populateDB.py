#Use this script to populate the |restaurant| table of the database

import mysql.connector
import json
from decimal import *
import random

#Define database variables
DATABASE_USER = 'root'
DATABASE_HOST = '127.0.0.1'
DATABASE_NAME = 'lectures'

#Create connection to MySQL
cnx = mysql.connector.connect(user=DATABASE_USER, host=DATABASE_HOST, database=DATABASE_NAME)
cursor = cnx.cursor()

#Load json file
inputFile = open('restaurantData.json','r')
restaurantDict = json.load(inputFile)
inputFile.close()

#Dictionary to convert JSON day format to appropriate input to the database
jsonDay = {'Monday':'M','Tuesday':'T','Wednesday':'W','Thursday':'TH','Friday':'F','Saturday':'S','Sunday':'SU'}

#Loop through the restaurants and add info and menu to database
for key, restaurant in restaurantDict.iteritems():
	
	###############################
	## Add restaurant info first ##
	###############################

	inputDict = {
		'restId' : key,
		'name' : restaurant['name'],
		'address' : restaurant['street_address'],
		'city' : restaurant['locality'],
		'state' : restaurant['region'],
		'zip' : restaurant['postal_code'],
		'phone' : restaurant['phone'],
		'lat' : restaurant['lat'],
		'lng' : restaurant['long'],
		'url' : restaurant['website_url']
	}

	#Insert this info into the database
	addRestaurant = ("INSERT INTO restaurants (restId, name, address, city, state, zip, phone, lat, lng, url) VALUES (%(restId)s,  %(name)s, %(address)s, %(city)s, %(state)s, %(zip)s, %(phone)s, %(lat)s, %(lng)s, %(url)s)")
	cursor.execute(addRestaurant,inputDict)

	###############################
	## Add hours table into dtb ##
	###############################
	for day in ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday']:
		if restaurant['open_hours'][day] != []:
			for i in range(len(restaurant['open_hours'][day])):
				hours = restaurant['open_hours'][day][i].split(" - ")
				inputHoursDict = {
					'restId' : key,
					'day' : jsonDay[day],
					'open' : hours[0],
					'close' : hours[1]
				}

				#Insert hours for restaurants with non-null open and close hours 
				addHours = ("INSERT INTO hours (restId, day, open, close) VALUES (%(restId)s,  %(day)s, %(open)s, %(close)s)")
				cursor.execute(addHours, inputHoursDict)	

	###############################
	## Add restaurant info first ##
	###############################
	
	for i in range(len(restaurant['menus'])):
		# print restaurant['menus'][i]['menu_name']
		inputMenuDict = {
			'restId' : key,
			'menuName' : restaurant['menus'][i]['menu_name']
		}
		addMenu = ("INSERT INTO categories (restId, menuName) VALUES (%(restId)s, %(menuName)s)")
		cursor.execute(addMenu, inputMenuDict)


	###############################
	## Add menuitems 
	###############################
	for i in range(len(restaurant['menus'])):
		for j in range(len(restaurant['menus'][i]['sections'])):
			for k in range(len(restaurant['menus'][i]['sections'][j]['subsections'])):
				if (len(restaurant['menus'][i]['sections'][j]['subsections']) != 0):
					for l in range(len(restaurant['menus'][i]['sections'][j]['subsections'][k])):
						for m in range(len(restaurant['menus'][i]['sections'][j]['subsections'][k]['contents'])):
							
							def getName():
								if ('name' in restaurant['menus'][i]['sections'][j]['subsections'][k]['contents'][m].keys()):
									nameValue = restaurant['menus'][i]['sections'][j]['subsections'][k]['contents'][m]['name']
								else:
									nameValue = "null"
								return 

							def getPrice():
								if ('price' in restaurant['menus'][i]['sections'][j]['subsections'][k]['contents'][m].keys()):
									priceValue = restaurant['menus'][i]['sections'][j]['subsections'][k]['contents'][m]['price']
								else:
									priceValue = "null"
								return priceValue

							def getDescription():
								if ('description' in restaurant['menus'][i]['sections'][j]['subsections'][k]['contents'][m].keys()):
									descValue = restaurant['menus'][i]['sections'][j]['subsections'][k]['contents'][m]['description']
								else:
									descValue = "null"
								return descValue

							inputMenuItemsDict ={
								'restId' : key,
								'menuName' : restaurant['menus'][i]['menu_name'],
								'name' : getName(),
								'price' : getPrice(),
								'description' : getDescription()
							}	

							addMenuItems = ("INSERT INTO menuitems (restId, menuName,name,price,description) VALUES (%(restId)s, %(menuName)s, %(name)s, %(price)s, %(description)s)")
							cursor.execute(addMenuItems, inputMenuItemsDict)


cnx.commit()
cnx.close()